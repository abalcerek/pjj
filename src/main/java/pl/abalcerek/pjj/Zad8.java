package pl.abalcerek.pjj;

import java.util.Arrays;
import java.util.Random;

public class Zad8 {

    public static void main(String[] args) {
        int[] arr1 = generateRandomArray();
        int[] arr2 = generateRandomArray();

        System.out.println("Arr1: " + Arrays.toString(arr1));
        System.out.println("Arr2: " + Arrays.toString(arr2));

        //initialize arrays of size equal to sum of sizes of both arrys
        int bigArraySize = arr1.length + arr2.length;

        // initialize empty array to hold value from both arrays
        int[] concatenationArray = new int[bigArraySize];

        // fill elements from first array
        for (int i = 0; i < arr1.length; i++) {
            concatenationArray[i] = arr1[i];
        }

        // fill elements from second array
        for (int i = 0; i < arr2.length; i++) {
            // we are skipping first arr1.length elements as they had been already added in the first loop
            concatenationArray[i + arr1.length] = arr2[i];
        }

        System.out.println("Array that is concatenation of both arrays: " + Arrays.toString(concatenationArray));

        // find max and min element in concat table
        int currentMax = concatenationArray[0];
        int currentMin = concatenationArray[0];
        for (int i = 0; i < concatenationArray.length; i++) {
            if (currentMax < concatenationArray[i]) {
                currentMax = concatenationArray[i];
            }

            if (currentMin > concatenationArray[i]) {
                currentMin = concatenationArray[i];
            }
        }

        // after this loop currentMin and currentMax are real min and max from this table and as fallows both input tables
        System.out.println("Min and max value from both table are respectivaly: " + currentMin + ", " + currentMax);

        // create array with all elements between currentMin and current Max

        // calculate length of the array
        int len = currentMax - currentMin;

        // allocate array
        int[] minMaxArray = new int[len];

        for (int i = 0; i < minMaxArray.length; i++) {
            minMaxArray[i] = currentMin + i;
        }

        System.out.println("Result array holding elements between min and max: " + Arrays.toString(minMaxArray));

    }


    private static int[] generateRandomArray() {
        Random random = new Random();

        // generate length of the array
        int len = random.nextInt(10);

        // initialize array
        int[] tab = new int[len];

        // populate array with random values
        for (int i = 0; i < tab.length; i++) {
            int randomInt  = random.nextInt(30);
            tab[i] = randomInt;
        }

        return tab;
    }

}
