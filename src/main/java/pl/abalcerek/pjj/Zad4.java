package pl.abalcerek.pjj;

import java.util.Arrays;

public class Zad4 {

    public static void main(String[] args) {

        //both of these are equivalent
        int[] tab1 = {1, 2, 3};
        int tab[] = {1, 2, 3};

        System.out.println(Arrays.toString(tab1));
        System.out.println(Arrays.toString(tab));

        // this loop iterates over all elements
        for (int i = 0; i < tab.length; i++) {
            // this loop first time will iterate over all elements, second time over all but first, third over all but first two (that is only last element)
            for (int j = i; j < tab.length; j++) {
                System.out.println(tab[i] - tab[j]);
            }
        }

        // first outer loop iteration

        //outer loop i = 0 inner loop j = i = 0 -> tab[i] = 1, tab[j] = 1 -> tab[i] - tab[j] = 0
        //outer loop i = 0 inner loop j = i + 1 = 0 + 1 = 1 -> tab[i] = 1, tab[j] = 2 -> tab[i] - tab[j] = -1
        //outer loop i = 0 inner loop j = i + 2 = 0 + 1 = 1 -> tab[i] = 1, tab[j] = 3 -> tab[i] - tab[j] = -2

        // second outer loop iteration

        //outer loop i = 1 inner loop j = i = 1 -> tab[i] = 2, tab[j] = 2 -> tab[i] - tab[j] = 0
        //outer loop i = 1 inner loop j = i + 1 = 1 + 1 = 2 -> tab[i] = 2, tab[j] = 3 -> tab[i] - tab[j] = -2

        // second outer loop iteration

        //outer loop i = 2 inner loop j = i = 2 -> tab[i] = 3, tab[j] = 3 -> tab[i] - tab[j] = 0
    }

}
