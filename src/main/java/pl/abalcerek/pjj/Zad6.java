package pl.abalcerek.pjj;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zad6 {

    public static void main(String[] args) {
        Random random = new Random();

        // we need to know how many letters there is so we can initiate the table
        char[] letters = new char[26];

        // we can iterate over the letters as over numbers as they have numeric representation in java and are numbered alphabetically
        int i = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            letters[i] = c;
            i++;
        }

        // initialize random letter array
        char[] randomLetters = new char[5];

        for (int j = 0; j < randomLetters.length; j++) {
            // generate random index from 0 to 25 to pic letter from letters array
            int randomIndexOfLetterArray = random.nextInt(26);randomLetters[j] = letters[randomIndexOfLetterArray];
        }

        System.out.println("Random 5 letter array: " + Arrays.toString(randomLetters));

        // create scanner object
        Scanner scanner = new Scanner(System.in);

//        boolean runProgramLoop = true;

        // start infinit loop it will go forever if not stopped by break statement
        while (true) {
            // check if all elements are zero
            boolean allZeros = true;
            for (int j = 0; j < randomLetters.length; j++) {
                if (randomLetters[j] != 0) {
                    allZeros = false;
                }
            }
            if (allZeros) {
                break;
            }

            // if some letters are still not zero we continue with our program

            // print current state of random letter array
            System.out.println("Current state of randomLetters: " + Arrays.toString(randomLetters));

            // read character from the consol to variable c;
            char c = scanner.next().charAt(0);

            // count the number of the occurrences of inputted character;
            int numberOfOccurrences = 0;
            for (int j = 0; j < randomLetters.length; j++) {
                if (c == randomLetters[j]) {
                    numberOfOccurrences += 1;
                    // replace this character with zero
                    randomLetters[j] = 0;
                }
            }

            System.out.println("Inserted character has occurred " + numberOfOccurrences + " times");
        }

        System.out.println("No more nonzero characters existing the program.");

    }

}
