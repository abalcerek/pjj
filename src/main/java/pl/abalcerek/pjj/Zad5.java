package pl.abalcerek.pjj;

public class Zad5 {

    public static void main(String[] args) {
        byte[] tab1 = {0, 1, 1, 1, 0};
        byte[] tab2 = {0, 1, 1, 1, 0};

        // if we have evidence that they are not equal we gonna change it to false;
        boolean areEqual = true;
        // if arrays have different size they for sure are not equal
        if (tab1.length != tab2.length) {
            areEqual = false;
        }

        // if we do not already know it is not equal we gonna check elements
        if (areEqual) {
            for (int i = 0; i < tab1.length; i++) {
                if (tab1[i] != tab2[i]) {
                    areEqual = false;
                }
            }
        }

        System.out.println("tables are equal: " + areEqual);
    }

}
