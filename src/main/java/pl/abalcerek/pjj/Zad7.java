package pl.abalcerek.pjj;

import java.util.Arrays;
import java.util.Random;

public class Zad7 {

    public static void main(String[] args) {
        // initialize random array
        int[] tab = new int[5];
        Random random = new Random();

        for (int i = 0; i < tab.length; i++) {
            int randomInt  = random.nextInt(30);
            tab[i] = randomInt;
        }

        System.out.println("Input array: " + Arrays.toString(tab));

        // sort the array
        Arrays.sort(tab);

        System.out.println("Sorted array: " + Arrays.toString(tab));

        // if array is sorted then we need only check the differences between two consecutive elements

        // index of first element with smaller difference
        int index = 0;
        // for now difference of the first two elements (!!! we assume that the array has at least two elements otherwise will get error and this exercise does not have any sense)
        int minDifference = tab[1] - tab[0];
        // we gonna access two elements so dont want to iterate up to last one as it will be check with second last
        for (int i = 0; i < tab.length - 1; i++) {
            if (tab[i + 1] - tab[i] < minDifference) {
                minDifference = tab[i + 1] - tab[i];
                index = i;
            }
        }

        // after this loop elements with smallest difference should be at indexes index and index + 1
        System.out.println("Element with mallest difference are: " + tab[index] + " and " + tab[index + 1]);

    }

}
