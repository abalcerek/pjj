package pl.abalcerek.pjj;

import java.util.Arrays;
import java.util.Random;

public class Zad2 {

    public static void main(String[] args) {

        double[] doubleArray = new double[10];

        Random random = new Random();

        for (int i = 0; i < doubleArray.length; i++) {

            // random.nextDouble() generates doubles between 0 and 1 if we want the bigger ones then we have to multiple it
            // we need bigger values then 1 because otherwise all integral parts would be 0
            double doubleValue = random.nextDouble() * 1000;

            doubleArray[i] += doubleValue;
        }

        System.out.println("Content of doubleArray: " + Arrays.toString(doubleArray));

        System.out.println("Elements on even indexes:");
        for (int i = 0; i < doubleArray.length; i++) {
            if (i % 2 == 0) {
                System.out.println(doubleArray[i]);
            }
        }

        System.out.println("Elements with odd integral part:");
        for (int i = 0; i < doubleArray.length; i++) {

            // I assume that converting to int means taking integral part
            int integralPart = (int) doubleArray[i];

            if (integralPart % 2 == 1) {
                System.out.println(doubleArray[i]);
            }
        }


    }

}
