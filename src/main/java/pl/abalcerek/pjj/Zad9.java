package pl.abalcerek.pjj;

import java.util.Scanner;

public class Zad9 {

    // using this formula: https://pl.wikipedia.org/wiki/Tr%C3%B3jk%C4%85t_Pascala

    public static void main(String[] args) {

        // initialize scanner object that allows to consume user input
        Scanner scanner = new Scanner(System.in);

        // consume single int value
        int n = scanner.nextInt();

        System.out.println("Pascal triangle of degree: " + n);

        // we gonna use two dimensional array to represent triangle of n degree has n + 1 rows

        // allocate two dimensional array with n rows
        int[][] triangle = new int[n + 1][];

        // fill triangle array

        // fill first row
        triangle[0] = new int[]{1};

        // fill the rest base on the wikipedia formula
        for (int i = 1; i < triangle.length; i++) {

            // initialize i-th row it should have i + 1 elements
            int[] row = new int[i + 1];

            // use wikipedia formula to calculate this row form previous one

            // first and last element have to be treated separately as the previous rows are shorter and we would go beyond the array and got exception
            row[0] = triangle[i - 1][0];
            row[row.length - 1] = triangle[i - 1][row.length - 2];

            for (int j = 1; j < row.length - 1; j++) {
                row[j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
            }

            // put one dimensional array row in to two dimensional array triangle
            triangle[i] = row;
        }

        //print triangle
        for (int i = 0; i < triangle.length; i++) {
            for (int j = 0; j < triangle[i].length; j++) {
                System.out.print(triangle[i][j] + " ");
            }
            System.out.println();
        }

    }

}
