package pl.abalcerek.pjj;

public class Zad5withFunctions {

    public static void main(String[] args) {
        byte[] tab01 = {0, 1, 1, 1, 0};
        byte[] tab02 = {0, 1, 1, 1, 0};

        System.out.println("tables are equal: " + areEqual(tab01, tab02));

        // now if we have function we can reuse it and calculate two examples
        byte[] tab11 = {1, 0, 1, 1, 0};
        byte[] tab12 = {0, 1, 1, 1, 0};

        System.out.println("tables are equal: " + areEqual(tab11, tab12));


    }

    private static boolean areEqual(byte[] t1, byte[] t2) {

        // if tables have different length function can stop and return false we know they are not equal
        if (t1.length != t2.length) {
            return false;
        }

        for (int i = 0; i < t1.length; i++) {
            // if tables are different on any element we know they are different function can stop and return false
            if (t1[i] != t2[i]) {
                return false;
            }
        }

        // at this point we compared length and every element and there is no differences that means they are even and we return true
        return true;
    }

}
