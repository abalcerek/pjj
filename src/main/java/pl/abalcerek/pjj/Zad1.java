package pl.abalcerek.pjj;

import java.util.Arrays;
import java.util.Random;

public class Zad1 {

    public static void main(String[] args) {
        int[] tab = new int[10];
        System.out.println("the length of the tab table is qual to: " + tab.length);

        Random random = new Random();

        int numberOfOnce = 0;
        for (int i = 0; i < tab.length; i++) {
            // generate random positive int that is smaller then 2 and assign it to temporary variable to reuse in
            // next to statements
            int randomInt  = random.nextInt(2);
            // assign generated value to the element of array tab with index i
            tab[i] = randomInt;
            // notice that it is enough to sum all elements of the table as zeros will not count.
            // do it now to avoid multiple loops
            numberOfOnce += randomInt;
        }

        System.out.println("Generate array tab: " + Arrays.toString(tab));
        System.out.println("There is: " + numberOfOnce + " once in array tab");
    }

}
