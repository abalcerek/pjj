package pl.abalcerek.pjj;

public class Zad3 {

    public static void main(String[] args) {
        // characters in java are ordered you can compare them like numbers

        // that's how you can declare array literal
        char[] chars = {'a', '1', 'c'};

        System.out.println(chars);

        // solution 1: for only 3 element array

        int minimalIndex;
        if (chars[0] < chars[1] && chars[0] < chars[2]) {
            minimalIndex = 0;
        } else if (chars[1] < chars[2] && chars[1] < chars[0]) {
            minimalIndex = 1;
        } else {
            minimalIndex = 3;
        }

        System.out.println("Solution 1: Index of element with minimal value is: " + minimalIndex);


        // solution 2: for array of arbitrary length
        int minimalIndex2 = 0;
        char minimalValue = chars[0];
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] < minimalValue) {
                minimalIndex2 = i;
                minimalValue = chars[i];
            }
        }

        System.out.println("Solution 2: Index of element with minimal value is: " + minimalIndex2);
    }

}
